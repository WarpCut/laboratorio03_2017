package it.unimi.di.sweng.lab03;

public class IntegerList {
	private IntegerNode head = null;
	private IntegerNode tail = null;
	
	public IntegerList(){
		
	}
	
	public IntegerList(String str){
		if(str.equals("")){
			head = tail = null;
		}else{
			for(int i = 0; i < str.length(); i++){
				System.out.println((int)(str.charAt(i)));
				if(isNumber(str, i)){
					if(head == null){
						head = tail = new IntegerNode(getNumber(str, i));
					}else{
						IntegerNode node = new IntegerNode(getNumber(str, i));
						tail.setNext(node);
						tail = node;
					}
					i++;
				}else{
					throw new IllegalArgumentException();
				}
			}
		}
	}

	private int getNumber(String str, int i) {
		return (int)(str.charAt(i) - 48);
	}

	private boolean isNumber(String str, int i) {
		return ((int)(str.charAt(i))) <= 57 && ((int)(str.charAt(i))) >= 48;
	}
	
	public String toString(){
		StringBuilder result = new StringBuilder("[");
		int i = 0;
		IntegerNode currentNode = head;
		while(currentNode != null){
			if(i++ > 0){
				result.append(' ');
			}
			result.append(currentNode.getValue());
			currentNode = currentNode.next();
		}
		return result.append(']').toString();
	}
	
	public void addLast(int value) {
		if(head == null){
			head = tail = new IntegerNode(value);
		}else{
			IntegerNode node = new IntegerNode(value);
			tail.setNext(node);
			tail = node;
		}
	}
	
	public void addFirst(int value){
		if(head ==  null){
			head = tail = new IntegerNode(value);
		}else{
			IntegerNode node = new IntegerNode(value);
			node.setNext(head);
			head = node;
		}
	}
	
}
