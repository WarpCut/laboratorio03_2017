package it.unimi.di.sweng.lab03;

public class IntegerNode{
	private Integer val = null;
	private IntegerNode next = null;
	
	public IntegerNode(int i){
		this.val = i;
	}
	
	public IntegerNode next() {
		return next;
	}
	
	public void setNext(IntegerNode integerNode){
		next = integerNode;
	}
	
	public int getValue() {
		return val;
	}
}
